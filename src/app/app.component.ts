import { Component } from "@angular/core";
import { DatatableResources } from "datatable";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "example";

  data = [
    { name: "ragaca 1", age: "ragaca 2", gender: "ragaca 3" },
    { name: "ragaca 4", age: "ragaca 5", gender: "ragaca 6" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" },
    { name: "ragaca 9", age: "ragaca 10", gender: "ragaca 11" }
  ];

  itemResource = new DatatableResources(this.data);

  items: any[] = [];
  count: number;

  constructor() {}

  loadData(params) {
    this.itemResource.count().then(count => this.count = count);
    this.itemResource.query(params).then(items => (this.items = items));
  }

  onSearch(searchText) {
  }

  onNewItem() {
  }
}
