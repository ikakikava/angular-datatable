export interface DatatableParams {
    limit?: number;
    offset?: number;
    sortBy?: string;
    sortAsc?: boolean;
}