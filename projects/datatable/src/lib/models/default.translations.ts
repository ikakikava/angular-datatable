import { DatatableTranslations } from "./datatable.translation";

export const defaultTranslations: DatatableTranslations = {
    paginationText: 'Showing {from} to {to} of {count} entries'
}