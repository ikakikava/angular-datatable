import { DatatableParams } from "./params.interface";

export class DatatableResources<T> {
  constructor(private items: T[]) {}

  query(params: DatatableParams, filter?: (item: T, index: number, items: T[]) => boolean): Promise<T[]> {

    let result: T[] = [];
    if (filter) {
      result = this.items.filter(filter);
    } else {
      result = this.items.slice();
    }

    if (params.offset !== undefined) {
      if (params.limit === undefined) {
        result = result.slice(params.offset, result.length);
      } else {
        result = result.slice(params.offset, params.offset + params.limit);
      }
    }

    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(result));
    });
  }

  count(): Promise<number> {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(this.items.length));
    });

  }
}
