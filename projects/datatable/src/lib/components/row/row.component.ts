import { Component, OnDestroy, Input, Inject, forwardRef } from "@angular/core";
import { DatatableComponent } from "../datatable/datatable.component";

@Component({
  selector: '[datatable-row]',
  templateUrl: './row.component.html',
  styleUrls: ['../../styles/shared.style.css']
})
export class DatatableRowComponent implements OnDestroy {
  @Input() item: any;

  constructor(@Inject(forwardRef(() => DatatableComponent)) public datatable: DatatableComponent ) {
  }
  
  ngOnInit() {
  }

  ngOnDestroy(): void {
    
  }
}
