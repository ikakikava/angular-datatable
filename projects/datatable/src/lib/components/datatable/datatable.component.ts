import {
  Component,
  OnInit,
  Input,
  Output,
  ContentChildren,
  QueryList,
  EventEmitter,
  OnDestroy
} from "@angular/core";
import { DatatableColumnDirective } from "../../directives/column.directive";
import { DatatableParams } from "../../models/params.interface";
import { Subject, Subscription, pipe } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { DatatableTranslations } from "../../models/datatable.translation";
import { defaultTranslations } from "../../models/default.translations";

@Component({
  selector: "bom-datatable",
  templateUrl: "./datatable.component.html",
  styleUrls: ["../../styles/shared.style.css", "./datatable.style.css"]
})
export class DatatableComponent implements OnInit, OnDestroy {
  @Input() title: string = "";
  @Input() items: any[] = [];
  @Input() labels: DatatableTranslations;

  private _itemCount;

  @Input()
  get itemCount(): number {
    return this._itemCount;
  }

  set itemCount(value: number) {
    this._itemCount = value;
  }

  // Start Datatable Param Inputs
  private _offset = 0;

  @Input()
  get offset(): number {
    return this._offset;
  }

  set offset(value: number) {
    this._offset = value;
    this.initDataSubject.next();
  }

  private _limit = 10;

  @Input()
  get limit(): number {
    return this._limit;
  }

  set limit(value: number) {
    this._limit = value;
    this.initDataSubject.next();
  }
  // End Datatable Param Inputs

  @ContentChildren(DatatableColumnDirective) columns: QueryList<DatatableColumnDirective>;

  @Output() initDatatable = new EventEmitter();

  initDataSubject = new Subject<void>();
  initDataSubscription: Subscription;

  constructor() {}

  ngOnInit() {
    this.initData();

    this.labels = { ...defaultTranslations };

    this.initDataSubscription = this.initDataSubject.pipe(debounceTime(100)).subscribe(() => this.initData());
  }

  initData() {
    this.initDatatable.emit(this.initParams());
  }

  initParams(): DatatableParams {
    const params = <DatatableParams>{};

    params.offset = this.offset;
    params.limit = this.limit;

    return params;
  }

  get maxPage() {
    return Math.ceil(this.itemCount / this.limit);
  }

  //For Search in Header (EventEmitter)
  @Output() onSearch = new EventEmitter();

  //For Add New Button in Header (EventEmitter)
  @Output() onNewItem = new EventEmitter();
  
  ngOnDestroy() {
    this.initDataSubscription.unsubscribe();
  }
}
