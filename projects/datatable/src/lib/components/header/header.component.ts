import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  OnDestroy,
  OnInit,
  Inject,
  forwardRef
} from "@angular/core";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { DatatableComponent } from "../datatable/datatable.component";

@Component({
  selector: "datatable-header",
  templateUrl: "./header.component.html",
  styleUrls: ["../../styles/shared.style.css", "./header.style.css"]
})
export class DatatableHeaderComponent implements OnInit, OnDestroy {
  @Input() title: string = "";
  @ViewChild("search") search: ElementRef;

  private _searchSubject = new Subject<string>();
  private _searchText: string = "";

  constructor(@Inject(forwardRef(() => DatatableComponent)) public datatable: DatatableComponent) {

  }

  ngOnInit(): void {
    this._searchSubject.pipe(debounceTime(400)).subscribe(result => {
      if (this._searchText != result) {
        this._searchText = result;
        this.datatable.onSearch.emit({ searchText: this._searchText, params: this.datatable.initParams() });
      }
    });
  }

  initSearch(value: string) {
    this._searchSubject.next(value);
  }

  initNewItem() {
    this.datatable.onNewItem.emit(null);
  }

  ngOnDestroy(): void {
    this._searchSubject.unsubscribe();
  }
}
