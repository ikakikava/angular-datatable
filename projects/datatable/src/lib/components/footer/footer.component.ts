import { Component, OnInit, Inject, forwardRef, Input } from "@angular/core";
import { DatatableComponent } from "../datatable/datatable.component";

@Component({
  selector: "datatable-footer",
  templateUrl: "footer.component.html",
  styleUrls: ["../../styles/shared.style.css", "./footer.style.css"]
})
export class DatatableFooterComponent implements OnInit {
  paginationText: string;
  currentPage: number = 1;
  Math: any;
  private _pages = [];
  etc = "...";

  constructor(@Inject(forwardRef(() => DatatableComponent)) public datatable: DatatableComponent) {
    this.Math = Math;
  }

  ngOnInit() {
  }

  previousPage() {
    this.datatable.offset -= Math.min(this.datatable.limit, this.datatable.offset);
    this.currentPage -= 1;
  }

  nextPage() {
    this.datatable.offset += this.datatable.limit;
    this.currentPage += 1;
    // var etcIndex = this._pages.indexOf(this.currentPage) + 1;
    // if(this._pages[etcIndex] == this.etc) {
    //   this._pages[etcIndex] = this.currentPage + 1;
    //   this.pages;
    // }
  }

  get pages() {
    this._pages = []
    for(var i = 1; i <= this.datatable.maxPage; i++) {
      if(i <= 2 || (i > this.datatable.maxPage - 1))
      {
        this._pages.push(i)
      }
      else if(!this._pages.includes(this.etc)) {
        this._pages.push(this.etc);
      }
    }
    return this._pages;
  }

  changePage(pageNumber) {
    // if(this.etc == pageNumber) {
    //   var index = this._pages.indexOf(pageNumber);
    //   console.log(this._pages[index - 1]);
    // }
    if(this.currentPage != pageNumber) {
      this.datatable.offset = this.datatable.limit * (pageNumber - 1);
      this.currentPage = pageNumber;
    }
  }
}
