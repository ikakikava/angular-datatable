import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";

//Datatable Library Components
import { DatatableComponent } from "./components/datatable/datatable.component";
import { DatatableHeaderComponent } from "./components/header/header.component";
import { DatatableFooterComponent } from "./components/footer/footer.component";
import { DatatableRowComponent } from "./components/row/row.component";
import { DatatableColumnDirective } from "./directives/column.directive";
import { DatatableResources } from "./models/datatable.resources";

export {
  DatatableResources
}

@NgModule({
  imports: [CommonModule],
  declarations: [
    DatatableComponent,
    DatatableHeaderComponent,
    DatatableFooterComponent,
    DatatableRowComponent,
    DatatableColumnDirective
  ],
  exports: [DatatableComponent, DatatableColumnDirective]
})
export class DatatableModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: DatatableModule,
      providers: []
    };
  }
}
