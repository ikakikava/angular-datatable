import { Directive, Input, ElementRef, ContentChild } from "@angular/core";

@Directive({
  selector: 'datatable-column'
})

export class DatatableColumnDirective {

  @Input() name: string;
  @Input() property: string;
  @Input() class: number | string;

  @ContentChild('template') template: ElementRef;

}
